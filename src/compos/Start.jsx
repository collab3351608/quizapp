import React, { useEffect, useState } from "react";
import Quizcard from "./quizcard";

export default function Start({ settotalqustions, totalqustion, data }) {
  const [start, setstart] = useState(false);
  const [total, setcors] = useState([]);
  const [submit, setsubmit] = useState(false);

  useEffect(() => {
    console.log(totalqustion, "here are these");
  }, [totalqustion]);

  return (
    <div>
      {!start ? (
        <div className="startbtn">
          <select
            name="totalq"
            id="total"
            onChange={(e) => settotalqustions(e.target.value)}
          >
            <option value="">select total qustion</option>
            <option value="5">5</option>
            <option value="10">10</option>
          </select>
          <button disabled={totalqustion == ""} onClick={(e) => setstart(true)}>
            Start Quiz
          </button>
        </div>
      ) : (
        <></>
      )}

      {start ? (
        <>{submit ? <h1>total points :{total.length}</h1> : <></>}</>
      ) : (
        <></>
      )}
      {start ? (
        <Quizcard
          data={data}
          setcors={setcors}
          total={total}
          setsubmit={setsubmit}
          submit={submit}
        />
      ) : (
        <></>
      )}
    </div>
  );
}
