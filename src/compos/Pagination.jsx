import React, { useEffect } from "react";

export default function Pagination({
  setpage,
  page,
  totalqustion,
  isOptionSelected,
  setoption,
}) {
  useEffect(() => {
    console.log(page, "changin page");
  }, [page]);
  const handleprepage = () => {
    if (page == 1) {
      //   console.log("u cannot pre more ");
    } else if (page > 1) {
      setpage(page - 1);
    }
  };
  const handleNextPage = () => {
    setoption(true);
    if (page == totalqustion) {
      //   console.log("u cannot go further ");
    } else if (page < totalqustion) {
      setpage(page + 1);
    }
  };

  return (
    <div className="pagination">
      <button onClick={handleprepage}>Pre</button>
      <button onClick={handleNextPage} disabled={isOptionSelected}>
        Next
      </button>
    </div>
  );
}
