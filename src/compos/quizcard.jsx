import React, { useContext, useEffect, useState } from "react";
import { Context } from "../App";
import Pagination from "./Pagination";
import { AiOutlineCheck } from "react-icons/ai";

export default function ({ data, setcors, total, submit, setsubmit }) {
  const [isOptionSelected, setoption] = useState(true);
  const totalqustion = useContext(Context);
  const [page, setpage] = useState(1);
  const [lastQustion, setlastqustion] = useState("");
  const checkAnser = (e, answer, index) => {
    setlastqustion(index + 1);
    let selectedAns = e.target.value;
    if (selectedAns) setoption(false);
    if (selectedAns == answer) {
      let arr = [...total];
      arr.push(1);
      setcors(arr);
    }
  };

  const handleSubmit = () => {
    setsubmit(true);
  };

  useEffect(() => {
    console.log("lastqust", lastQustion, "totalqust", totalqustion);
  }, [lastQustion]);

  return (
    <>
      {!submit &&
        data &&
        data.map((val, index) => {
          if (index == page) {
            return (
              <div key={index} className="qustion">
                <h1>{val.qustion}</h1>
                <select onChange={(e) => checkAnser(e, val.answer, index)}>
                  <option value="">select option </option>
                  {val?.options?.map((vals, j) => {
                    return <option value={vals}>{vals}</option>;
                  })}
                </select>
              </div>
            );
          }
        })}

      {lastQustion && lastQustion == totalqustion ? (
        <>
          <button className="subbtn" onClick={handleSubmit}>
            Submit {submit ? <AiOutlineCheck /> : <></>}
          </button>
        </>
      ) : (
        <Pagination
          setpage={setpage}
          totalqustion={totalqustion}
          page={page}
          isOptionSelected={isOptionSelected}
          setoption={setoption}
        />
      )}
    </>
  );
}
