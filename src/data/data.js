export default data = [
  {
    id: 1,
    qustion: "What country has the highest life expectancy?",
    answer: "Hong Kong",
    options: ["Hong Kong", "India", "Pakistan", "USA"],
  },
  {
    id: 2,
    qustion: "What year was the United Nations established?",
    answer: "1945",
    options: ["1945", "1947", "1948", "1935"],
  },
  {
    id: 3,
    qustion: "How many elements are in the periodic table? ",
    answer: "118",
    options: ["118", "120", "122", "125"],
  },
  {
    id: 4,
    qustion: "Aureolin is a shade of what color?",
    answer: "Yellow",
    options: ["Yellow", "blue", "green", "black"],
  },
  {
    id: 5,
    qustion: "What country has won the most World Cups? ",
    answer: "Brazil",
    options: ["Brazil", "India", "Pakistan", "USA"],
  },
];
